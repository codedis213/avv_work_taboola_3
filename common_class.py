import requests
from bs4 import BeautifulSoup
import os


class HideMyass(object):
    """
    this class does the following
    * collect the proxies from hide my ass server
    """

    def __init__(self):
        # self.link_hidmyass = "http://proxylist.hidemyass.com/search-1302598#listable"
        self.link_hidmyass = "http://proxylist.hidemyass.com/search-1302598#listable"
        self.base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.html_hidemyass = os.path.join(self.base_dir, 'mapping_blog', 'html_hidemyass.html')

    def open_link(self):
        r = requests.get(self.link_hidmyass)
        self.page = r.content
        r.close()

    def save_page(self):
        f = open(self.html_hidemyass, "w+")
        page = self.page
        f.write(page)
        f.close()



    def give_req_class(self, txt):
        txt = str(txt).strip()
        if "{display:none}" not in txt and txt:
            culr_start = txt.find("{")
            return txt[0:culr_start]



    def scrap_page_hms(self):
        """this function will parse the ip from td/ string """

        # soup = BeautifulSoup(self.page, "html.parser")
        f = open(self.html_hidemyass)
        page = f.read()
        f.close()

        soup = BeautifulSoup(page, "html.parser")
        tr_list = soup.find("table").find("tbody").find_all("tr")

        for tr in tr_list[-1:]:
            require_td = tr.find_all("td")[1] # it contain the ip which has 8080 port
            req_string_to_parse = require_td.get_text().split("\n")[-1] # string to parse
            print req_string_to_parse

            style_text = require_td.find("style").get_text().split(".")

            req_class = map(self.give_req_class, style_text)
            req_class = filter(None, req_class) # this class contain numbers of ip

            all_span = require_td.find_all(["span", "div"])

            zeroth_span = all_span[0]
            span_list_from_other = all_span[1:]

            over_all_span = span_list_from_other

            string_not_req = []
            string_req = []

            for sp in over_all_span:
                ip_text = sp.get_text()
                ip_text_len = len(ip_text)

                res_idx = req_string_to_parse.find(ip_text)

                if res_idx != 0:
                    string_req.append(req_string_to_parse[:ip_text_len])



                req_string_to_parse = req_string_to_parse[ip_text_len:]
                print ip_text
                print req_string_to_parse



                style_text = sp.get("style")
                class_text = sp.get("class")

                class_int = False
                int_val = False

                try:
                    int_val = int(str(class_text[0]))
                    class_int = True

                except:
                    pass

                if (style_text and style_text == "display: inline"):
                    string_not_req.append(None)
                    string_req.append(ip_text)

                elif (style_text and style_text == "display:none"):
                    string_not_req.append(ip_text)
                    string_req.append(None)

                elif (class_text and class_text[0] in req_class) or int_val:
                    string_not_req.append(None)
                    string_req.append(ip_text)

                elif class_text and class_text[0] not in req_class:
                    string_not_req.append(ip_text)
                    string_req.append(None)

            print filter(None, string_req)
            print filter(None, string_not_req)

            print req_string_to_parse
            print "*"*20





if __name__=="__main__":
    obj= HideMyass()
    # obj.open_link()
    # obj.save_page()
    obj.scrap_page_hms()




