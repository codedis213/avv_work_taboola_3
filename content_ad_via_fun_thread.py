from req_module import *
from bs4 import BeautifulSoup
import xlrd
from xlrd.sheet import ctype_text
import phan_proxy
import re
from urlparse import urlparse
from threading import Thread
from Queue import Queue
import time
import logging
import multiprocessing

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )
thread_quantity = 10
th_queue = Queue()

multi_process_quantity = 10
mp_queue = multiprocessing.JoinableQueue()

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DIR_REQ = os.path.join(BASE_DIR, 'avv_work_taboola_3')
FNAME = os.path.join(DIR_REQ, 'proxies.txt')


def str_strip(x):
    try:
        x = str(x).strip()
    except:
        x = x.encode('ascii', "ignore").decode('utf-8').strip()
    return x


class MyThread(object):
    """
    obj = MyThread(5)
    iter_seq = list(range(10))
    obj.start_thread(iter_seq, obj.work_on_elem)
    """

    thread_quantity = 5
    enclosure_queue2 = Queue()

    def __init__(self, f, row_n_cols, thread_quantity=5):
        MyThread.thread_quantity = thread_quantity
        self.t_list = []
        self.f = f
        self.row_n_cols = row_n_cols
        self.flag = 1

    def start_thread(self, iter_seq, fun_name):
        thread_quantity = MyThread.thread_quantity
        enclosure_queue2 = MyThread.enclosure_queue2
        t_list = self.t_list

        for i in range(thread_quantity):
            t_list.append(Thread(target=self.target_thread, args=(i, enclosure_queue2)))
            t_list[-1].start()

        for elem in iter_seq:
            enclosure_queue2.put((elem, fun_name))

        enclosure_queue2.join()

        for p in t_list:
            enclosure_queue2.put(None)

        enclosure_queue2.join()

        for p in t_list:
            p.join(180)

    def target_thread(self, i, q):
        for elem, fun_name in iter(q.get, None):
            try:
                if self.flag:
                    fun_name(elem)
            except:
                pass

            time.sleep(2)
            q.task_done()

        q.task_done()

    def page_to_link(self, url):
        f = self.f
        row_n_cols = self.row_n_cols

        try:
            req_sec_url = url
            url_parse = urlparse(req_sec_url)

            if not url_parse.netloc:
                req_sec_url = "http://%s%s" % (str(row_n_cols[0]).strip(), req_sec_url)

            pattern_url = str(row_n_cols[0]).strip()

            if pattern_url[:pattern_url.find(".")] not in str(req_sec_url):
                return

            try:
                driver = phan_proxy.main(str(req_sec_url).strip())
                page = driver.page_source
                driver.delete_all_cookies()
                driver.quit()
            except:
                page = False

            if page:
                try:
                    soup = BeautifulSoup(page, "html.parser")
                    taboola_str = soup.find("div", attrs={"class": "trc_rbox"})
                    outbrain_str = soup.find("div", attrs={"class": "OUTBRAIN"})
                    colombiaSuccess = soup.find("div", attrs={"class": re.compile("colombiaSuccess")})
                    content_ad = soup.find("div", attrs={"class":"ac_adbox"})

                    # logging.debug((taboola_str, outbrain_str, colombiaSuccess, content_ad))

                    if content_ad:
                        add_id = content_ad.get("id")
                        add_class = content_ad.get("class")
                        if add_class:
                            add_class = " ".join(list(add_class))
                        add_type = "content_ad"

                    elif taboola_str:
                        parent_div = taboola_str.find_parent("div", attrs={"class":"trc_related_container"})
                        add_id = parent_div.get("id")
                        add_class = parent_div.get("class")
                        if add_class:
                            add_class = " ".join(list(add_class))
                        add_type = "taboola"

                    elif outbrain_str:
                        parent_div = outbrain_str.find_parent("div")
                        add_id = parent_div.get("id")
                        add_class = parent_div.get("class")
                        if add_class:
                            add_class = " ".join(list(add_class))
                        add_type = "outbrain"

                    # elif colombiaSuccess:
                    #     add_id = colombiaSuccess.get("id")
                    #     add_type = "colombiaSuccess"

                    else:
                        add_id = False
                        add_class = False

                    # logging.debug((add_id, add_class))

                    if add_id:
                        add_id = str_strip(add_id)
                        req_list = [row_n_cols[0], add_type, "id", add_id, req_sec_url,] + row_n_cols[1:]

                        f.write(", ".join(req_list) + "\n")
                        logging.debug((add_id, add_class))
                        logging.debug("%s ==== scraped" % row_n_cols[0])
                        self.flag = 0

                    elif add_class:
                        add_id = str_strip(add_id)
                        req_list = [row_n_cols[0], add_type, "class", add_class, req_sec_url,] + row_n_cols[1:]

                        f.write(", ".join(req_list) + "\n")
                        logging.debug((add_id, add_class))
                        logging.debug("%s ==== scraped" % row_n_cols[0])
                        self.flag = 0

                    logging.debug("i am here......................")
                except:
                    pass
        except:
            pass

    def work_on_elem(self, elem):
        logging.debug(elem)


def parse_home_page(f, row_n_cols):
    url = "http://www.%s/" % row_n_cols[0]
    row_n_cols = map(str_strip, row_n_cols)

    try:
        driver = phan_proxy.main(url)
        page = driver.page_source
        driver.delete_all_cookies()
        driver.quit()
    except:
        page = False

    if page:
        try:
            soup = BeautifulSoup(page, "html.parser")
            all_a = []

            # ul_menu = soup.find("div", class_=re.compile(".*menu.*"))
            #
            # if ul_menu:
            #     all_a = [str(a.get("href")).strip() for a in ul_menu.find_all("a")]
            #
            # elif soup.find("div", class_=re.compile(".*nav.*")):
            #     ul_menu = soup.find("div", class_=re.compile(".*nav.*"))
            #     all_a = [str(a.get("href")).strip() for a in ul_menu.find_all("a")]
            #
            # elif soup.find("div", class_=re.compile(".*top.*")):
            #     ul_menu = soup.find("div", class_=re.compile(".*top.*"))
            #     all_a = [str(a.get("href")).strip() for a in ul_menu.find_all("a")]
            #
            # elif soup.find("ul"):
            #     ul_menu = soup.find("ul")
            #     all_a = [str(a.get("href")).strip() for a in ul_menu.find_all("a")]
            a_list = soup.find_all("a")
            all_a = [str(a.get("href")).strip() for a in a_list]

            obj = MyThread(f, row_n_cols, 10)
            iter_seq = all_a
            logging.debug(all_a)
            obj.start_thread(iter_seq, obj.page_to_link)

        except:
            pass


def target_multi_process(i, q):
    for elem, fun_name in iter(q.get, None):
        try:
            file_name = "content_ad_vai_fun_second%s.csv" % i
            with open(file_name, 'a+') as f:
                fun_name(f, elem)
        except:
            pass

        time.sleep(2)
        q.task_done()

    q.task_done()


def multi_process_roll():
    f_name = os.path.join(DIR_REQ, "SimilarTechReportContent.ad.xlsx")
    xl_workbook = xlrd.open_workbook(f_name)
    xl_sheet = xl_workbook.sheet_by_index(0)

    t_list = []

    for i in range(multi_process_quantity):
        t_list.append(multiprocessing.Process(target=target_multi_process, args=(i, mp_queue,)))
        t_list[-1].start()

    for idx in range(1, xl_sheet.nrows):
        row_n_cols = xl_sheet.row_values(idx)
        # row_n_cols = ['iflscience.com', 'News & Media', "214.0", "175500000.0", "0.1875"]
        logging.debug(row_n_cols)
        mp_queue.put((row_n_cols, parse_home_page))

    logging.debug(" mq join start ")
    mp_queue.join()
    logging.debug(" mq join end ")

    for p in t_list:
        mp_queue.put(None)

    mp_queue.join()

    for p in t_list:
        p.join(600)


if __name__ == "__main__":
    multi_process_roll()
    # f = open("testing_file.csv", "w+")
    # row_n_cols = ['buzzfil.net', 'News & Media', "214.0", "175500000.0", "0.1875"]
    # parse_home_page(f, row_n_cols)
    # f.close()