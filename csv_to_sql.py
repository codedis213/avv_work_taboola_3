#!/usr/bin/python

import MySQLdb
import glob
from urlparse import urlparse


def csv_to_database():
    # Open database connection
    db = MySQLdb.connect("localhost", "root", "rootavv", "add_collection")

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # Create table as per requirement
    sql = """CREATE TABLE IF NOT EXISTS add_div(\
    Extra_id int(11) NOT NULL AUTO_INCREMENT,\
    type text  NOT NULL,\
    value text  NOT NULL,\
    domain_name varchar(256) DEFAULT NULL UNIQUE,\
    ip_address text DEFAULT NULL,\
    publish_by text  NOT NULL,\
    dte_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\
    PRIMARY KEY (Extra_id)\
    )"""

    cursor.execute(sql)

    directory_name = "/root/work_by_jai/avv_work_taboola_3"
    taboola_pattern = "%s/taboola_vai_fun_second*.csv" % directory_name

    taboola_file_list =  glob.glob(taboola_pattern)

    for taboola_file in taboola_file_list:
        f = open(taboola_file)
        for line in f:
            try:
                # opednews.com, taboola, id, taboola-below-main-column, http://opednews.com/populum/diarypage.php?did=11337, Arts & Entertainment > Movies, 132839.0, 200000.0, 0.861111104488
                line_list = map(str.strip, filter(None, line.split(",")))
                # domain_name = line_list[0]
                add_type = line_list[1]
                attribute_type = line_list[2]
                value = line_list[3]
                url = line_list[4]
                url_parse = urlparse(url)
                domain_name = url_parse.netloc

                sql = """INSERT INTO add_div(domain_name,
                         type, publish_by, value, ip_address)
                         VALUES ('%s', '%s', '%s', '%s', '%s')"""
                sql = sql % (domain_name, add_type, attribute_type, value, url)

                try:
                   # Execute the SQL command
                   cursor.execute(sql)
                   # Commit your changes in the database
                   db.commit()
                   print "inserted....................................."
                except:
                   # Rollback in case there is any error
                   db.rollback()
                   print "rollback....................................."
            except:
                pass


    content_ad_pattern = "%s/content_ad_vai_fun_second*.csv" % directory_name

    content_ad_file_list =  glob.glob(content_ad_pattern)

    for content_ad_file in content_ad_file_list:
        with open(content_ad_file) as f:
            # f = open(content_ad_file)
            for line in f:
                try:
                    # opednews.com, taboola, id, taboola-below-main-column, http://opednews.com/populum/diarypage.php?did=11337, Arts & Entertainment > Movies, 132839.0, 200000.0, 0.861111104488
                    line_list = map(str.strip, filter(None, line.split(",")))
                    # domain_name = line_list[0]
                    add_type = line_list[1]
                    attribute_type = line_list[2]
                    value = line_list[3]
                    url = line_list[4]
                    url_parse = urlparse(url)
                    domain_name = url_parse.netloc

                    sql = """INSERT INTO add_div(domain_name,
                             type, publish_by, value, ip_address)
                             VALUES ('%s', '%s', '%s', '%s', '%s')"""
                    sql = sql % (domain_name, add_type, attribute_type, value, url)

                    try:
                       # Execute the SQL command
                       cursor.execute(sql)
                       # Commit your changes in the database
                       db.commit()
                       print "inserted....................................."
                    except:
                       # Rollback in case there is any error
                       db.rollback()
                       print "rollback....................................."
                except:
                    pass

    outbrain_pattern = "%s/outbrain_vai_fun_second*.csv" % directory_name

    outbrain_file_list =  glob.glob(outbrain_pattern)

    for outbrain_ad_file in outbrain_file_list:
        with open(outbrain_ad_file) as f:
            # f = open(content_ad_file)
            for line in f:
                try:
                    # opednews.com, taboola, id, taboola-below-main-column, http://opednews.com/populum/diarypage.php?did=11337, Arts & Entertainment > Movies, 132839.0, 200000.0, 0.861111104488
                    line_list = map(str.strip, filter(None, line.split(",")))
                    # domain_name = line_list[0]
                    add_type = line_list[1]
                    attribute_type = line_list[2]
                    value = line_list[3]
                    url = line_list[4]
                    url_parse = urlparse(url)
                    domain_name = url_parse.netloc

                    sql = """INSERT INTO add_div(domain_name,
                             type, publish_by, value, ip_address)
                             VALUES ('%s', '%s', '%s', '%s', '%s')"""
                    sql = sql % (domain_name, add_type, attribute_type, value, url)

                    try:
                       # Execute the SQL command
                       cursor.execute(sql)
                       # Commit your changes in the database
                       db.commit()
                       print "inserted....................................."
                    except:
                       # Rollback in case there is any error
                       db.rollback()
                       print "rollback....................................."
                except:
                    pass

    # disconnect from server
    db.close()


if __name__=="__main__":
    csv_to_database()
